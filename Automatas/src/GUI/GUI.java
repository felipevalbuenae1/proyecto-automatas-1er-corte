package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import Mundo.Automata;

public class GUI extends JFrame {
	
	public JPanelBotones panelBotones;
	public JPanelTexto panelTexto;
	
	//Conexion con mundo
	public Automata automata;

	public GUI(Automata conectAutomata) {
		
		this.automata = conectAutomata;
		// TODO Auto-generated constructor stub
		setTitle("Automatas");
		setSize(300, 300);
		setResizable(true);
		setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		
		panelTexto = new JPanelTexto();
		add(panelTexto,BorderLayout.CENTER);
		
		panelBotones = new JPanelBotones(this);
		add(panelBotones,BorderLayout.SOUTH);
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Automata conect = new Automata();
		GUI run = new GUI(conect);
		run.setVisible(true);
		
	}

	public void CapturarExpresion() {
		// TODO Auto-generated method stub
		
		automata.setNumEntero(panelTexto.getNumEntero());
		automata.setNumReal(panelTexto.getNumReal());
		automata.setNotCientifica(panelTexto.getNotCientifica());
		automata.setBinario(panelTexto.getBinario());
		automata.setEmail(panelTexto.getNumReal());
		automata.darExpresiones();
		
	}

}
