package Mundo;

import javax.swing.JOptionPane;

public class Automata {

	public String expNumEntero;
	public String expNumReal;
	public String expNotCientifica;
	public String expBinario;
	public String expEmail;
	
	public Automata() {
		// TODO Auto-generated constructor stub
			
	}

	public void setNumEntero(String numEntero ) {
		expNumEntero = numEntero;
	}
	
	public void setNumReal(String numReal) {
		expNumReal = numReal;
	}
	
	public void setNotCientifica(String notCientifica ) {
		expNotCientifica = notCientifica;
	}
	
	public void setBinario(String Binario ) {
		expBinario = Binario;
	}
	
	public void setEmail(String Email ) {
		expEmail = Email;
	}
	
	public void darExpresiones() {
		System.out.println("Numero Entero: "+expNumEntero);
		System.out.println("Numero Real: "+expNumReal);
		System.out.println("Notacion cientifica: "+expNotCientifica);
		System.out.println("Binario: "+expBinario);
		System.out.println("Email: "+expEmail);
	}
}
