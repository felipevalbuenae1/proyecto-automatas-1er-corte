package Mundo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Automata {

	public String expNumEntero;
	public String expNumReal;
	public String expNotCientifica;
	public String expBinario;
	public String expEmail;
	
	public Automata() {
		
	}

	public Automata(String expNumEntero, String expNumReal, String expNotCientifica, String expBinario,
			String expEmail) {
		super();
		this.expNumEntero = expNumEntero;
		this.expNumReal = expNumReal;
		this.expNotCientifica = expNotCientifica;
		this.expBinario = expBinario;
		this.expEmail = expEmail;
	}



	public boolean validarNumEntero() {
            
            Pattern expresionRegular = Pattern.compile("^-?[0-9]+$");
            Matcher validacion = expresionRegular.matcher(expNumEntero);
            if(validacion.matches()){
                
               return true;      
             
            }else{
                
               return false;   
             
            }
	}
	
	public boolean validarNumReal() {
            //([-|+][0-9]*|[0-9]*)|([-|+][0-9]*+[.|,][0-9]*|[0-9]*[.|,][0-9]*)
            
            Pattern expresionRegular = Pattern.compile("^-?[0-9]+([\\.,][0-9]+)?$");
            Matcher validacion = expresionRegular.matcher(expNumReal);
            if(validacion.matches()){   
               return true;      
             
            }else{
                
               return false;   
             
            }
	}
	
	public boolean setNotCientifica() {
		 Pattern expresionRegular = Pattern.compile("[-+]?[0-9]*\\.?[0-9]+(\\s|([eE]))+(\\s|([-+]?[0-9]))+?");
         Matcher validacion = expresionRegular.matcher(expNotCientifica);
         if(validacion.matches()){
            return true;      
          
         }else{
             
            return false;   
          
         }
	}
	
	public void setBinario(String Binario ) {
		expBinario = Binario;
	}
	
	public void setEmail(String Email ) {
		expEmail = Email;
	}
	
	/*public void darExpresiones() {
		System.out.println("Numero Entero: "+expNumEntero);
		System.out.println("Numero Real: "+expNumReal);
		System.out.println("Notacion cientifica: "+expNotCientifica);
		System.out.println("Binario: "+expBinario);
		System.out.println("Email: "+expEmail);
	}*/
}
