package Controlador;

import Mundo.Automata;

public class Solicitud {
	
	public Automata automata;//Conexion con mundo
	public String expNumEntero;
	public String expNumReal;
	public String expNotCientifica;
	public String expBinario;
	public String expEmail;
	
	/*
	 * Constructor vacio de la clase Solicitud 
	 */
	public Solicitud() {
		
	}

	
	/*
	 * Constructor sobre cargardo de la clase Solicitud
	 * @param String expNumEntero, String expNumReal, String expNotCientifica, String expBinario, String expEmail
	 */
	public Solicitud(String expNumEntero, String expNumReal, String expNotCientifica, String expBinario,
			String expEmail) {
		
		this.expNumEntero = expNumEntero;
		this.expNumReal = expNumReal;
		this.expNotCientifica = expNotCientifica;
		this.expBinario = expBinario;
		this.expEmail = expEmail;
	}
	
	/*
	 * 
	 */
	public void Validar_Automata(){
		
		automata = new Automata(expNumEntero, expNumReal, expNotCientifica, expBinario, expEmail);
                System.out.println("Respuesta numero ENTERO "+automata.validarNumEntero()); 
                System.out.println("Respuesta numero REAL "+automata.validarNumReal()); 
                System.out.println("Respuesta notacion cientifica "+automata.setNotCientifica()); 
	}
	
	
	
	

}
