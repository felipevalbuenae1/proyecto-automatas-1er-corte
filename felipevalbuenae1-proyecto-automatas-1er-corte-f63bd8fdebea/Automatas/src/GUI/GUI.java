package GUI;
import Controlador.Solicitud;
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import Controlador.Solicitud;

public class GUI extends JFrame {
	
	public JPanelBotones panelBotones;
	public JPanelTexto panelTexto;
	public Solicitud solicitud;
	
	String a[];

	public GUI() {
		
		setTitle("Automatas");
		setSize(300, 300);
		setResizable(true);
		setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		
		panelTexto = new JPanelTexto();
		add(panelTexto,BorderLayout.CENTER);
		
		panelBotones = new JPanelBotones(this);
		add(panelBotones,BorderLayout.SOUTH);
		
	}

	public static void main(String[] args) {
		GUI run = new GUI();
		run.setVisible(true);
	}
	
	public void enviarSolicitud() {
		
		a = panelTexto.getTextFields();
		solicitud = new Solicitud(a[0], a[1], a[2], a[3], a[4]);
		System.out.println("Numero Entero: "+a[0]);
		System.out.println("Numero Real: "+a[1]);
		System.out.println("Notacion cientifica: "+a[2]);
		System.out.println("Binario: "+a[3]);
		System.out.println("Email: "+a[4]);
		solicitud.Validar_Automata();
		
	}
	



}
