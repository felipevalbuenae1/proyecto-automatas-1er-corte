package GUI;

import java.awt.GridLayout;
import Controlador.Solicitud;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import Mundo.Automata;

public class JPanelTexto extends JPanel{
	
	//Texto de las diferentes cadenas de caracteres que valida el programa
	public JLabel labelNumEntero;
	public JLabel labelNumReal;
	public JLabel labelNotCientifica;
	public JLabel labelBinario;
	public JLabel labelEmail;
	
	//Campos de ingreso de datos
	public JTextField tFieldNumEntero;
	public JTextField tFieldNumReal;
	public JTextField tFieldNotCientifica;
	public JTextField tFieldBinario;
	public JTextField tFieldEmail;
	
	public JPanelTexto() {
		
		setBorder(new TitledBorder("Panel numeros"));
		setLayout(new GridLayout(5,2));
		
		labelNumEntero = new JLabel("Numero Entero:");
		labelNumReal = new JLabel("Numero Real:");
		labelNotCientifica = new JLabel("Notacion Cientifica:");
		labelBinario = new JLabel("Binario:");
		labelEmail = new JLabel("Correo Electronico:");
		
		tFieldNumEntero = new JTextField();
		tFieldNumReal = new JTextField();
		tFieldNotCientifica = new JTextField();
		tFieldBinario = new JTextField();
		tFieldEmail = new JTextField();
		
		add(labelNumEntero);
		add(tFieldNumEntero);
		
		add(labelNumReal);
		add(tFieldNumReal);
		
		add(labelNotCientifica);
		add(tFieldNotCientifica);
		
		add(labelBinario);
		add(tFieldBinario);
		
		add(labelEmail);
		add(tFieldEmail);				
	}

        
	public String[] getTextFields() {
		
		String textFields[] = {tFieldNumEntero.getText(), tFieldNumReal.getText(), tFieldNotCientifica.getText(), tFieldBinario.getText(), tFieldEmail.getText()};
		return textFields;
	}
	
}
