package GUI;

import java.awt.GridLayout;
import Controlador.Solicitud;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;


public class JPanelBotones extends JPanel implements ActionListener{
	
	private  static final String BotonCommand="Validar";
	private JButton BotonComvertir;
	
	public Solicitud solicitud;
	
	//Conexion a la GUI
	public GUI conect;
	
	
	public JPanelBotones(GUI conect) {
		
		this.conect = conect;
		
		setLayout(new GridLayout(1,1));
		setBorder(new TitledBorder("Panel Botones"));
		BotonComvertir = new JButton("Validar");
		BotonComvertir.setActionCommand(BotonCommand);
		BotonComvertir.addActionListener(this);
		add(BotonComvertir);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		String comand = arg0.getActionCommand();
		if (comand.equals("Validar")) {
			
			conect.enviarSolicitud();
			
		}
		
	}

}
